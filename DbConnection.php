<?php

/**
 * Created by Studyx.
 * User: xavierdekeyster
 * Date: 13/11/16
 * Time: 09:30
 *
 * Database connection using Singleton pattern
 */
class DbConnection
{

    private static $_instance;
    private $_connection; //The single instance
    private $_host = "192.168.10.10";
    private $_username = "homestead";
    private $_password = "secret";
    private $_database = "masterclass1";

    /*
    Get an instance of the Database
    @return Instance
    */

    /*private function __construct()
    {
        $this->_connection = new mysqli($this->_host, $this->_username,
            $this->_password, $this->_database);

        // Error handling
        if (mysqli_connect_error()) {
            trigger_error("Failed to connect to MySQL: " . mysql_connect_error(),
                E_USER_ERROR);
        }
    }*/


    private function __construct() {
        try {

            $this->_connection = new PDO("mysql:host=$this->_host;dbname=$this->_database",
                "$this->_username", "$this->_password");

        } catch (PdoException $e) {

            echo 'Error: '.$e->getMessage();

        }
    }

    public static function getInstance()
    {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }



    // Magic method clone is empty to prevent duplication of connection

    public function getConnection()
    {
        return $this->_connection;
    }

    // Get mysqli connection

    private function __clone()
    {
    }

} 